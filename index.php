<!DOCTYPE html>
<html>
<head>
    <title>Image neural network with javascript</title>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/Network.js"></script>
    <script type="text/javascript" src="js/Neural.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <canvas id="draw" width="400px" height="250px"></canvas>
    <div id="middle">
        <button id="clearCanvas">Clear</button>
    </div>
    <canvas id="preview" width="400px" height="250px"></canvas>
    <div id="settings">
        <h3>Here be settings</h3>
        <form name="settingsForm" id="settingsForm">
            <label for="runTraining">Run simulations</label>
            <input type="checkbox" name="runTraining" id="runTraining" value="1">
            <label for="displayStatistics">Display statistics</label>
            <input type="checkbox" name="displayStatistics" id="displayStatistics" value="1">
        </form>
    </div>
</body>
</html>
